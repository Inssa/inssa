#!/usr/bin/env bash


############################################################################
#					 	PARTIE 2 : Serveur d'Autorité				       #
############################################################################


############################################################################
#				  Configurations pour le serveur "dwikiorg"			       #
############################################################################

himage dwikiorg mkdir -p /etc/named #Ici on crée le fichier named dans /etc

hcp dwikiorg/* dwikiorg:/etc/named/. 
# Ici on copie tout ce qu'il y'a dans le dossier locale dans dwikiorg, 
# et on colle ça dans la machine dwikiorg sous IMUNES

hcp dwikiorg/named.conf dwikiorg:/etc/.
#puis on fait named -c /etc/named.conf pour demarer le serveur


# Pour les commentaires ça s'applique pareil pour les autres


############################################################################
#					Configurations pour le serveur "diutre"			       #
############################################################################

himage diutre mkdir -p /etc/named
hcp diutre/* diutre:/etc/named/.
hcp diutre/named.conf diutre:/etc/.

himage diutre mkdir -p /tmp/www
hcp diutre/index.html diutre:/tmp/www/.; 
#puis on fait named -c /etc/named.conf



############################################################################
#					Configurations pour le serveur "drtiutre"			   #
############################################################################

himage drtiutre mkdir -p /etc/named
hcp drtiutre/* drtiutre:/etc/named/.
hcp drtiutre/named.conf drtiutre:/etc/.
#puis on fait named -c /etc/named.conf

############################################################################
#					 FIN PARTIE 2 : Serveur d'Autorité				       #
############################################################################


##############################################################################################


############################################################################
#					 	PARTIE 3 : Top Level Domain 				       #
############################################################################


############################################################################
#					Configurations pour le serveur "dorg"			       #
############################################################################

himage dorg mkdir -p /etc/named
hcp dorg/* dorg:/etc/named/.
hcp dorg/named.conf dorg:/etc/.
#puis on fait named -c /etc/named.conf



############################################################################
#					Configurations pour le serveur "dre"			       *
############################################################################

himage dre mkdir -p /etc/named
hcp dre/* dre:/etc/named/.
hcp dre/named.conf dre:/etc/.
#puis on fait named -c /etc/named.conf

############################################################################
#					 FIN PARTIE 3 : Top Level Domain 				       #
############################################################################


##############################################################################################


############################################################################
#					 		 PARTIE 4 : Root Server 				       #
############################################################################

############################################################################
#				Configurations pour le serveur "aRootServer"			   #
############################################################################

himage aRootServer mkdir -p /etc/named
hcp aRootServer/* aRootServer:/etc/named/.
hcp aRootServer/named.conf aRootServer:/etc/.
#puis on fait named -c /etc/named.conf

############################################################################
#					 		FIN PARTIE 4 : Root Server 				       #
############################################################################


##Config pc1

hcp pc1/resolv.conf pc1:/etc/.
#cette commande copie le fichier resolv.conf dans /etc/resolv.conf de pc1

#Config pc2
hcp pc2/resolv.conf pc2:/etc/.
#cette commande copie le fichier resolv.conf dans /etc/resolv.conf de pc1



#Ici nous allons lancer un serveur web
#himage diutre mkdir -p /tmp/www
#hcp diutre/index.html diutre:/tmp/www; python -m SimpleHTTPServer 80

#Config pour pc1
